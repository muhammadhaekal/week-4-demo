const follower_list = document.getElementById("follower_list")

fetch("https://api.github.com/users/muhammadhaekal/followers")
    .then(response => {
        return response.json();
    })
    .then(datas => {
        console.log(datas)
        let temp = ""
        datas.forEach(data => {
            temp += `
            <div class="col-3 p-4">
                <div class="card">
                    <img class="card-img-top rounded-circle" src="${data.avatar_url}">
                    <div class="card-body">
                        <h5 class="card-title">${data.login}</h5>
                        <a href="${data.html_url}" target="_blank" class="btn btn-primary">Profile</a>
                    </div>
                </div>
            </div>
            `
            follower_list.innerHTML = temp
        });
    });