/* ----------------------------------------------------------------------------------- */
/* CREATE DATABASE  */
create database company;

/* ----------------------------------------------------------------------------------- */
/* CHANGE DATABASE  */
use  company;

/* ----------------------------------------------------------------------------------- */
/* CREATE TABLE  */
CREATE TABLE customers (
    id INT NOT NULL AUTO_INCREMENT,
    firstName VARCHAR(50),
    lastName VARCHAR(50),
    address VARCHAR(200),
    age INT(5),
    PRIMARY KEY (id)
);
/* ----------------------------------------------------------------------------------- */
/* INSERT TO TABLE (one data) */
insert into customers (firstName,lastName,email,address,city,state,zipcode,age) values
("John", "Doe","johndoe@gmail.com","55 Main st","Boston","Massachusetts","01221",23);

/* ----------------------------------------------------------------------------------- */
/* INSERT TO TABLE (more than one) */
insert into customers (firstName,lastName,email,address,city,state,zipcode,age) values
("Kathy", "Morris","kmorris@gmail.com","40 Willow st","Haverhill","Massachusetts","01860",45),
("Steven Samson", "Morris","ssamson@gmail.com","12 Gills Rd","Exeter","New Hampshire","01284",20),
("Lilian", "Davidson","liliand@gmail.com","7 Whittier st","Brooklyn","New York","34833",33),
("Derek", "Williams","dwill@gmail.com","477 Madison ct","Yonkers","New York","34993",64),
("Alan", "Carr","acarr@gmail.com","3436 Arizona Rd","vear","Rogaland","3095",55),
("Terry", "Flores","terry.flores66@gmail.com","6880 Hamilton Ave","varginha","piauí","42449",32),
("Blake", "Ennis","ben@gmail.com","1686 cedar st","Sandy Lake","Nova Scotia","O2X 0H8",64),
("Test", "Test","Test@gmail.com","Test cedar st","Sandy Lake","Test Scotia","Test 0H8",55);

/* ----------------------------------------------------------------------------------- */
/* UPDATE TABLE */
UPDATE customers 
SET 
    email = 'newemail@gmail.com'
WHERE
    id = 9;

/* ----------------------------------------------------------------------------------- */
/* DELETE TABLE */
DELETE FROM customers 
WHERE
    id = 9;
SELECT 
    *
FROM
    customers;

/* ----------------------------------------------------------------------------------- */
/* alter TABLE */
alter table customers add newCol varchar(255);
alter table customers modify column newCol INT(11);
alter table customers drop column newCol;

/* ----------------------------------------------------------------------------------- */
/* SELECT  */
select * from customers;
select firstName,lastName from customers;
select * from customers where id = 2;
select * from customers where firstname = "John";
/* USING ORDER  */
select * from customers order by lastName;
select * from customers order by lastName desc;
select city from customers order by firstName desc;
/* USING DISTINCT */
select distinct state from customers;
/* USING LOGICAL OPERATOR*/
select * from customers where age > 30;	
select * from customers where age between 20 and 40;
/* USING LIKE*/
select * from customers where city like "%on";
select * from customers where city like "%n%";
select * from customers where city not like "%n%";
/* USING IN */
select * from customers where state in ("New York", "New Hamshire");

/* ----------------------------------------------------------------------------------- */
/* INDEX  */

/* CREATE INDEX  */
create index CIndex on customers(city);  
drop index CIndex on customers;

/* ----------------------------------------------------------------------------------- */
/* FOREIGN KEY  */
create table products (
	id int not null auto_increment,
    name varchar(255),
    price int,
    primary key(id)
);

create table orders (
	id int not null auto_increment,
    orderNumber int,
    productId int,
    customerId int,
    orderDate datetime default current_timestamp,
    primary key(id),
    foreign key(customerId) references customers(id),
    foreign key(productId) references products(id)
);

/* ----------------------------------------------------------------------------------- */
/* INSERT  */

insert into products(name,price) values
("Product One", 10),
("Product Two", 5),
("Product Three", 65),
("Product Four", 45),
("Product Five", 100);


insert into orders(orderNumber, productId, customerId) values
(001,1,4),
(002,3,1),
(003,1,1),
(004,1,2),
(005,1,1),
(006,4,4),
(007,4,5),
(008,2,4),
(009,2,5);

use company;

/* ----------------------------------------------------------------------------------- */
/* SELECT FROM MULTIPLE TABLE  */
SELECT 
    c.firstName, c.lastName, o.id, o.orderNumber
FROM
    customers c,
    orders o
WHERE
    c.id = o.customerId;

/* ----------------------------------------------------------------------------------- */
/* SELECT FROM MULTIPLE TABLE (INNER JOIN) */

SELECT 
    c.firstName, c.lastName, o.id, o.orderNumber
FROM
    customers c
        LEFT JOIN
    orders o ON c.id = o.customerId
ORDER BY c.lastName;

/* ----------------------------------------------------------------------------------- */
/* SELECT FROM MULTIPLE TABLE (LEFT JOIN) */

SELECT 
    c.firstName, c.lastName, o.id, o.orderNumber, o.orderDate
FROM
    customers c
        LEFT JOIN
    orders o ON c.id = o.customerId
ORDER BY c.lastName;

/* ----------------------------------------------------------------------------------- */
/* SELECT FROM MULTIPLE TABLE (RIGHT JOIN) */

SELECT 
    c.firstName, c.lastName, o.id, o.orderNumber, o.orderDate
FROM
    customers c
        RIGHT JOIN
    orders o ON c.id = o.customerId
ORDER BY c.lastName;


/* ----------------------------------------------------------------------------------- */
/* SELECT FROM MULTIPLE TABLE */
SELECT 
    o.orderNumber, c.firstName, c.lastName, p.name
FROM
    orders o
        INNER JOIN
    products p ON o.productId = p.id
        INNER JOIN
    customers c ON o.customerId = c.id
ORDER BY o.orderNumber;

/* ----------------------------------------------------------------------------------- */
/* USING ALIASES */
SELECT 
    o.orderNumber as "Order Number", c.firstName "First name", c.lastName "Last Name", p.name "Product Name"
FROM
    orders o
        INNER JOIN
    products p ON o.productId = p.id
        INNER JOIN
    customers c ON o.customerId = c.id
ORDER BY o.orderNumber;

/* ----------------------------------------------------------------------------------- */
/* USING CONCAT() */
SELECT 
    CONCAT(firstName, ' ',lastName) 'Name',
    CONCAT(address,
            ' ',
            city,
            ' ',
            state,
            ' ',
            zipcode) 'Address'
FROM customers c;

/* ----------------------------------------------------------------------------------- */
/* SQL  FUNCTIONS */
select avg(age) from customers;
select count(*) from customers;
select max(age) from customers;
select min(age) from customers;
select sum(age) from customers;

/* COMBINE  SQL AGGREGATE FUNCTION WITH GROUP BY*/
select age, count(age)
from customers
where age > 30
group by age;




